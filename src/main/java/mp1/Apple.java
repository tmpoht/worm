package mp1;

import javafx.scene.paint.Color;

public class Apple {

	private Sijainti location;
	private Color color;
	private int pisteet;
	
	public Apple(Sijainti s, Color c, int tastiness) {
		location = s;
		color = c;
		pisteet = tastiness;
		
	}
	
	public Sijainti getLocation() {
		return location;
	}
	public Color getColor() {
		return color;
	}

	public int getPisteet() {
		return pisteet;
	}
}
