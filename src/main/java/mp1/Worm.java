package mp1;

import java.util.ArrayList;

public class Worm {

    private int length;
    private int direction;
    private int x;
    private int y;
    private double speed;
    public ArrayList<Sijainti> madonluvut;
    
    
    
    
    /**
     * suunta tulee olla 0, 90, 180 tai 270
     * x ja y tulee olla 10:llä jaollisia
     */
    public Worm(int pituus, int suunta, int x, int y, double nopeus){
        direction = suunta;
        length = pituus;
        this.x = x;
        this.y = y;
        speed = nopeus;
        madonluvut = new ArrayList<>();
    	

        if(direction > 270) {
        	direction = 0;
        }
        else if(direction < 0) {
        	direction = direction + 360;
        }
        
        switch(direction) {
        case 0:
            for (int i = length; i > 0; i--) {
            	madonluvut.add(new Sijainti(x,y - i*10));
            }
            break;
        case 90:
        	for (int i = length; i > 0; i--)  {
            	madonluvut.add(new Sijainti(x - i*10, y));
            }
            break;
        case 180:
        	for (int i = length; i > 0; i--) {
            	madonluvut.add(new Sijainti(x,y + i*10));
            }
            break;
        case 270:
        	for (int i = length; i > 0; i--) {
            	madonluvut.add(new Sijainti(x + i*10,y));
            }
            break;
        }

        madonluvut.add(new Sijainti(x,y));
    }

    public void turnUp(){
    	if(direction != 0 && direction != 180) {
    		direction = 0;
    	}
    }
    public void turnDown(){
    	if(direction != 180 && direction != 0) {
    		direction = 180;
    	}
    }
    public void turnRight(){
    	if(direction != 90 && direction != 270) {
    		direction = 90;
    	}	
    }
    public void turnLeft(){
    	if(direction != 270 && direction != 90) {
    		direction = 270;
    	}
    }

    public void grow(int pituus){
        length = length + pituus;
    }

    public void move(){
        switch(direction){
            case 0:
                y = y + 10;
                break;
            case 90:
                x = x + 10;
                break;
            case 180:
                y = y - 10;
                break;
            case 270:
                x = x - 10;
                break;
        }
        madonluvut.add(new Sijainti(x,y));
        madonluvut.remove(0);
    }
    
    public double getSpeed(){
        return speed;
    }
    public void setSpeed(int speed) {
    	this.speed = speed;
    }
    public int annaX(){
        return x;
    }
    public int annaY(){
        return y;
    }
    public int annaPituus() {
    	return length;
    }

}