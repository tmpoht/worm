package mp1;

import java.util.ArrayList;

public class Field {

	public ArrayList<Sijainti> seinat;
	
	public Field(ArrayList<Sijainti> seinat) {
		this.seinat = seinat;
	}

	/**
	 * oletuskonstruktori, pelkät seinät
	 */
	public Field() {
		seinat = new ArrayList<Sijainti>();
		for(int i = 0; i < 50; i++) {
			seinat.add(new Sijainti(i * 10, 0));
			seinat.add(new Sijainti(i * 10, 390));
		}
		for(int i = 0; i < 40; i++) {
			seinat.add(new Sijainti(0, i * 10));
			seinat.add(new Sijainti(490, i * 10));
			
		}
	}
	
	
}
