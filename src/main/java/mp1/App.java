package mp1;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;



// run by calling "mvn clean javafx:run" or "mvn compile exec:java"

public class App extends Application {

    Connection conn = null;
    
    public static void main(String[] args) {
        App.launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
    	
        BorderPane root = new BorderPane();
        root.setBackground(new Background(new BackgroundFill(Color.AQUAMARINE, new CornerRadii(1), new Insets(0))));
        
        
        VBox vbox = new VBox();
        vbox.setBackground(new Background(new BackgroundFill(Color.AQUA, new CornerRadii(1), new Insets(0))));
        
        Label pisteet = new Label("0");
        Button uusipeli = new Button("Uusi peli");
        TextField pelaaja = new TextField();
        pelaaja.setText("Pelaaja1");
        ComboBox kombo = new ComboBox();
        kombo.getItems().addAll(1,2,3,4,5);
        Button tulokset = new Button("Tulostaulu");
        
        
        vbox.getChildren().add(uusipeli);
        vbox.getChildren().add(pelaaja);
        vbox.getChildren().add(kombo);
        vbox.getChildren().add(new Label("Pisteet"));        
        vbox.getChildren().add(pisteet);
        vbox.getChildren().add(tulokset);
        
        vbox.setSpacing(40.0);
        vbox.setPrefWidth(100);
        vbox.setAlignment(Pos.CENTER);
        Canvas kanvas = new Canvas(500,400);
        kanvas.setFocusTraversable(true);
        uusipeli.setFocusTraversable(false);
        kombo.setFocusTraversable(false);
        tulokset.setFocusTraversable(false);
        pelaaja.setFocusTraversable(false);
        
        kombo.setValue(1);
        root.setLeft(vbox);
        root.setRight(kanvas);

        Game peli = new Game(kanvas, pisteet);

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/pisteet.db");
            System.out.println("Yhteys muodostettu");
        }
        catch(Exception ex) {
        	ex.printStackTrace();
        }

        uusipeli.setOnMouseClicked(e -> {	
        									if(uusipeli.getText() == "Uusi peli") {

        										peli.pelaa((int) kombo.getValue());
        										kombo.setDisable(true);
        										pelaaja.setDisable(true);
        										uusipeli.setText("Lopeta peli");}
        									
        									else {

        										Statement stat;
												try {
													stat = conn.createStatement();
													stat.execute("INSERT INTO SCORE VALUES('"+ pelaaja.getText().toString() +"'," + peli.annaPisteet() +"," + System.currentTimeMillis()+");");
													System.out.println("Tallennettu tulos tietokantaan!");
												} catch (SQLException e1) {
													e1.printStackTrace();
												}
        										peli.lopeta();
        										kombo.setDisable(false);

        										pelaaja.setDisable(false);
        										
        										uusipeli.setText("Uusi peli");
        										}
        									});

       tulokset.setOnMouseClicked(e -> 
       {
       		Dialog<Void> x = new Dialog<Void>();
       		x.setTitle("Tulostaulu");
       		x.setOnCloseRequest(lamb -> {
       							x.close();});
       		
       		Statement stat;
       		ResultSet rs;
			try {
				stat = conn.createStatement();
				rs = stat.executeQuery("SELECT * FROM SCORE ORDER BY SCORES DESC LIMIT 5 ;");
				int i = 1;
				String s = "";
				while(rs.next()) {
					
					s =  s + i +". "+ rs.getString(1) +", "+ rs.getInt(2) +" pistettä, "+ new SimpleDateFormat("dd.MM.YYYY, HH:mm").format(rs.getLong(3)) + "\n";
					i = i + 1;
				}
				
	       		x.setContentText(s);
	       		x.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);
	       		x.show();
	       			
	       		
       		} catch (Exception e1) {
       			e1.printStackTrace();
       		}
       		
    		   
    		   
       });
        Scene scene = new Scene(root);
        
        kanvas.setOnKeyPressed(new EventHandler<KeyEvent>() {
        	@Override
        	public void handle(KeyEvent event) {
        		switch (event.getCode()) {
        		case LEFT:
        			peli.annaMato().turnLeft();
        			break;
        		case RIGHT:
        			peli.annaMato().turnRight();	
        			break;
        		case UP:
        			peli.annaMato().turnDown();
        			break;
        		case DOWN:	
        			peli.annaMato().turnUp();
        			break;
				default:
					break;
        		}
        	}
        });
        
        stage.setOnCloseRequest(e ->{ 

	        try {
	        	conn.close();
	        	System.out.println("Yhteys suljettu");
	        }
	        catch(Exception ex) {
	        	ex.printStackTrace();
	        }
        });
        
        stage.setTitle("Matopeli");
        stage.setScene(scene);
        stage.show();
        
    }



}
