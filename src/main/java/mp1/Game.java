package mp1;

import java.util.ArrayList;
import java.util.Random;

import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;





public class Game {
	
	private Canvas kanvas;
	private Label pisteet;
    private int maara;
    private Worm mato;
    private int vaikeus;
    Random r = new Random();
    public boolean tila;
    
	public Game(Canvas kanvas, Label pisteet) {
		
		this.kanvas = kanvas;
		this.pisteet = pisteet;
		this.vaikeus = 1;
        
        tila = true;
		
	}
	
	public Worm annaMato(){
		return mato;
	}
	public void lopeta() {
		tila = false;
		
	}
	public int annaPisteet() {
		return maara;
	}
    public void pelaa(int vaikeeta){
    	
        tila = true;
    	pisteet.setText("0");

		maara = 0;

    	mato = new Worm(6,0,100,100,vaikeeta);
        Double v1 = r.nextDouble();
        Double v2 = r.nextDouble();
        Double v3 = r.nextDouble();

        Color p = new Color(v1, v2, v3, 1);
   
        
        kanvas.getGraphicsContext2D().setFill(p);
        

        Boolean peli = true;
        
        
        double aika = 1;
        
        Field field = new Field();
        
        ArrayList<Apple> omput = new ArrayList<Apple>();
                
        /*class Varjaa implements Runnable{

            @Override
            public void run() {

            }
            
        }*/

        class Piirra implements Runnable{

            @Override
            public void run() {

                kanvas.getGraphicsContext2D().clearRect(0, 0, 500, 400);
            	kanvas.getGraphicsContext2D().setFill(omput.get(0).getColor());
            	kanvas.getGraphicsContext2D().fillOval(omput.get(0).getLocation().xx, omput.get(0).getLocation().yy, 6, 6);
            	
            	kanvas.getGraphicsContext2D().setFill(p);
                kanvas.getGraphicsContext2D().fillOval(mato.annaX(), mato.annaY(), 7, 7);
                
                for(Sijainti x : mato.madonluvut) {
                	kanvas.getGraphicsContext2D().fillOval(x.xx, x.yy, 6, 6);
                }

                kanvas.getGraphicsContext2D().setFill(Color.BLUE);
                
                for(Sijainti x: field.seinat) {
                    kanvas.getGraphicsContext2D().fillRect(x.xx, x.yy, 9, 9);
                }
                

            }
        
        }

        class Looppi implements Runnable{

            Boolean b;
            double m;
            double aika;
            
            
            public Looppi(Boolean b, double nopeus, double aika){
                this.m = 6.0 - nopeus;
                this.b = b;
                this.aika = aika;
                omput.add(generateApple());
            }
            @Override
            public void run() {

                aika = aika *  m;
                while(tila){
                	
                    try{
                        Thread.sleep((long) aika * 80);
                    }
                    catch(Exception e){}
    
                    
                    //Platform.runLater(new Varjaa());
                    Platform.runLater(new Piirra());
                    testApple(omput);
                    if(testCollision()) {
                    	lopeta();
                    }
                    mato.move();
                }

                Platform.runLater(new Runnable() {
                	@Override
                	public void run(){
                		kanvas.getGraphicsContext2D().clearRect(0, 0, 500, 400);
                	}
                });
            }
        }
        Thread t = new Thread(new Looppi(peli, mato.getSpeed(), aika));
        t.start();

        
    }
    public void testApple(ArrayList<Apple> omenat) {
    	if(new Sijainti(mato.annaX(), mato.annaY()).equals(omenat.get(0).getLocation())) {
    		mato.grow(1);
    		mato.madonluvut.add(omenat.get(0).getLocation());
    		omenat.remove(0);
    		omenat.add(generateApple());
    		maara = maara + 1;
    		Platform.runLater(new Pisteyta());
    		
    		
    	}
    }
    public boolean testCollision() {
    	
    	Sijainti head = new Sijainti(mato.annaX(), mato.annaY()); 
    	
    	if(head.xx == 0 || head.xx == 490 || head.yy == 0 || head.yy == 390) {
    		return true;
    	}
    	for(int i = 0; i < mato.madonluvut.size() - 2; i++) {
    		
    		
    		if(mato.madonluvut.get(i).equals(head)){
    			return true;
    		}
    	}
    	return false;
  
    }
    public Apple generateApple() {
    	
    	int x1 = r.nextInt(48) * 10 + 10;
    	int y1 = r.nextInt(38) * 10 + 10;
    	
    	Apple apple = new Apple(new Sijainti(x1, y1), Color.RED, 1);
    	
    	
    	return apple;
    }

    class Pisteyta implements Runnable{

        @Override
        public void run() {
        	pisteet.setText(String.valueOf(maara));
        }
    
    }
}