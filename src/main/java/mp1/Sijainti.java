package mp1;



class Sijainti{
	
	public int xx;
	public int yy;
	public Sijainti(int xx, int yy) {
		this.xx = xx;
		this.yy = yy;
	}

	public boolean equals(Sijainti sijainti) {
		if(this.xx == sijainti.xx && this.yy == sijainti.yy) {
			return true;
		}
		return false;
	}
}